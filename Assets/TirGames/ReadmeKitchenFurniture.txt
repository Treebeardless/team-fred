Kitchen Furniture
V.1.1


Version history:
1.0 First release
1.1 Fixed smoothing groups, UV Map 


Package contains:
- 18 Kitchen models
- Prefabs, textures, materials
- Show room

email: tirgames.assets@gmail.com