﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class HandSwap : MonoBehaviour
{
    public VRTK_ControllerEvents HandEvents;

    public GameObject Default;
    public AudioSource BingSound;
    

    private bool _isDefault = false;

    private void Start()
    {
        Default.SetActive(false);
        
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        if (HandEvents != null)
        {
            HandEvents.ButtonTwoPressed += ToggleHands;

        }
    }

    void OnDisable()
    {
        if (HandEvents != null)
        {
            HandEvents.ButtonTwoPressed -= ToggleHands;
        }
    }

    void ToggleHands(object sender, ControllerInteractionEventArgs e)
    {
        if (_isDefault)
        {
            Default.SetActive(false);
           
        }
        else
        {
            Default.SetActive(true);
           
        }
        _isDefault = !_isDefault;

        BingSound.Play();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
