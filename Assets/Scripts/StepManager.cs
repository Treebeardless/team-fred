﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class DisplayEvent : UnityEvent<string> { }
[System.Serializable]
public class StepEvent : UnityEvent<int> { }
public class StepManager : MonoBehaviour
{
    public int CurrentStep = 0;
    public List<Step> StepList;
    public DisplayEvent OnDisplay;
    public StepEvent OnStepComplete;

    // Start is called before the first frame update
    void Start()
    {
        StepList = GetComponentsInChildren<Step>().ToList();

        foreach(Step step in StepList)
        {
            step.gameObject.SetActive(false);
        }
        StepList[0].gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ReceiveEvent(string key)
    {
        if (IsOutofBounds())
        {
            return;
        }

        if (StepList[CurrentStep].TargetKey == key)
        {
            OnStepComplete.Invoke(CurrentStep);
            StepList[CurrentStep].gameObject.SetActive(false);
            CurrentStep++;

            if (!IsOutofBounds())
            {
                StepList[CurrentStep].gameObject.SetActive(true);
            }
        }
    }

    bool IsOutofBounds()
    {
        return CurrentStep < 0 || CurrentStep >= StepList.Count;
    }
}
