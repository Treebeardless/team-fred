﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class EQListToggle : MonoBehaviour
{
    public VRTK_ControllerEvents HandEvents;

    public GameObject Default;
    public GameObject SwapObject;

    private bool _isDefault = true;

    private void Start()
    {
        Default.SetActive(true);
        SwapObject.SetActive(false);
    }

    // Start is called before the first frame update
    void OnEnable()
    {
        if (HandEvents != null)
        {
            HandEvents.ButtonOnePressed += ToggleHands;
        }
    }

    void OnDisable()
    {
        if (HandEvents != null)
        {
            HandEvents.ButtonOnePressed -= ToggleHands;
        }
    }

    void ToggleHands(object sender, ControllerInteractionEventArgs e)
    {
        if (_isDefault)
        {
            Default.SetActive(false);
            SwapObject.SetActive(true);
        }
        else
        {
            Default.SetActive(true);
            SwapObject.SetActive(false);
        }
        _isDefault = !_isDefault;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
