﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClipBoardController : MonoBehaviour
{

    public List<Toggle> StepToggles;

    // Start is called before the first frame update
    void Start()
    {
        foreach (Toggle t in StepToggles)
        {
            t.isOn = false;
        }
    }

    public void StepComplete(int stepIndex)
    {
        // 0, 1, 2, 3
        if (stepIndex < 0 || stepIndex >= StepToggles.Count)
        {
            return;
        }

        StepToggles[stepIndex].isOn = true;
    }
}
