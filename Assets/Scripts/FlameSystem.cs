﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameSystem : MonoBehaviour

{
    public List<ParticleSystem> Particles;

    // Start is called before the first frame update
    public void Play()
    {
        foreach (ParticleSystem p in Particles)
        {
            p.Play();
        }

    }

    // Update is called once per frame
    public void Stop()
    {
       foreach (ParticleSystem p in Particles)
        {
            p.Stop();
        }
    }
}
